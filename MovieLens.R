##########################################################
# Create edx and final_holdout_test sets 
##########################################################

# Note: this process could take a couple of minutes

if(!require(tidyverse)) install.packages("tidyverse", repos = "http://cran.us.r-project.org")
if(!require(caret)) install.packages("caret", repos = "http://cran.us.r-project.org")
if(!require(recosystem)) install.packages("recosystem", repos = "http://cran.us.r-project.org")
if(!require(lubridate)) install.packages("lubridate", repos = "http://cran.us.r-project.org")

library(tidyverse)
library(caret)
library(recosystem)
library(lubridate)

# MovieLens 10M dataset:
# https://grouplens.org/datasets/movielens/10m/
# http://files.grouplens.org/datasets/movielens/ml-10m.zip

options(timeout = 120)

dl <- "ml-10M100K.zip"
if(!file.exists(dl))
  download.file("https://files.grouplens.org/datasets/movielens/ml-10m.zip", dl)

ratings_file <- "ml-10M100K/ratings.dat"
if(!file.exists(ratings_file))
  unzip(dl, ratings_file)

movies_file <- "ml-10M100K/movies.dat"
if(!file.exists(movies_file))
  unzip(dl, movies_file)

ratings <- as.data.frame(str_split(read_lines(ratings_file), fixed("::"), simplify = TRUE),
                         stringsAsFactors = FALSE)
  colnames(ratings) <- c("userId", "movieId", "rating", "timestamp")
ratings <- ratings %>%
  mutate(userId = as.integer(userId),
         movieId = as.integer(movieId),
         rating = as.numeric(rating),
         timestamp = as.integer(timestamp))

movies <- as.data.frame(str_split(read_lines(movies_file), fixed("::"), simplify = TRUE),
                        stringsAsFactors = FALSE)
colnames(movies) <- c("movieId", "title", "genres")
movies <- movies %>%
  mutate(movieId = as.integer(movieId))

movielens <- left_join(ratings, movies, by = "movieId")

# Final hold-out test set will be 10% of MovieLens data
set.seed(1, sample.kind="Rounding") # if using R 3.6 or later
# set.seed(1) # if using R 3.5 or earlier
test_index <- createDataPartition(y = movielens$rating, times = 1, p = 0.1, list = FALSE)
edx <- movielens[-test_index,]
temp <- movielens[test_index,]

# Make sure userId and movieId in final hold-out test set are also in edx set
final_holdout_test <- temp %>% 
  semi_join(edx, by = "movieId") %>%
  semi_join(edx, by = "userId")

# Add rows removed from final hold-out test set back into edx set
removed <- anti_join(temp, final_holdout_test)
edx <- rbind(edx, removed)

rm(dl, ratings, movies, test_index, temp, movielens, removed)


####################################PROJECT##############################################################
# Test set will be 10% of edx data
set.seed(1, sample.kind="Rounding") # if using R 3.6 or later
test_index2 <- createDataPartition(y = edx$rating, times = 1, p = 0.1, list = FALSE)
train <- edx[-test_index2,]
temp <- edx[test_index2,]

# Make sure userId and movieId in test set are also in train set
test <- temp %>% 
  semi_join(train, by = "movieId") %>%
  semi_join(train, by = "userId")

# Add rows removed from test set back into train set
removed <- anti_join(temp, test)
train <- rbind(train, removed)



##############################################################################################################
# Just the average method

# calculate the overall average rating on the training dataset
mu <- mean(train$rating)

# predict all unknown ratings with mu and calculate the RMSE
rmse_avg <- RMSE(test$rating, mu)
rmse_avg

# Table to compare RSME
rmse_results1 <- data_frame(method = "The average", RMSE = rmse_avg)



##############################################################################################################
# Add the Movie effect 
str(train)
# Calculate Movie effects
movie_avgs <- train %>% 
  group_by(movieId) %>% 
  summarize(b_i = mean(rating - mu))

# Distribution of movie effects
movie_avgs %>% qplot(b_i, geom ="histogram", bins = 10, data = ., color = I("black"))

# Predict the ratings
predicted_ratings <- mu + test %>% 
  left_join(movie_avgs, by='movieId') %>%
  .$b_i

# Calculate RMSE
rmse_movie <- RMSE(predicted_ratings, test$rating)
rmse_movie

# Table to compare RSME
rmse_results1 <- bind_rows(rmse_results1,
                          data_frame(method="Movie Effect Model",
                                     RMSE = rmse_movie ))
rmse_results1 %>% knitr::kable()



##############################################################################################################
# Movie and add User effect
# Calculate User effects
user_avgs <- train %>% 
  left_join(movie_avgs, by='movieId') %>%
  group_by(userId) %>%
  summarize(b_u = mean(rating - mu - b_i))

user_avgs %>% qplot(b_u, geom ="histogram", bins = 10, data = ., color = I("black"))

# Predict the ratings
predicted_ratings <- test %>% 
  left_join(movie_avgs, by='movieId') %>%
  left_join(user_avgs, by='userId') %>%
  mutate(pred = mu + b_i + b_u) %>%
  .$pred

# Calculate RMSE
rmse_mov_user <- RMSE(predicted_ratings, test$rating)
rmse_mov_user

# Table to compare RSME
rmse_results1 <- bind_rows(rmse_results1,
                          data_frame(method="Movie + User Effects Model",  
                                     RMSE = rmse_mov_user ))
rmse_results1 %>% knitr::kable()



##############################################################################################################
# Movie, User and add date of rating effect
# Calculate date
train <- mutate(train, date = as_datetime(timestamp))
str(train)
test <- mutate(test, date = as_datetime(timestamp))

#Does it have a strong effect? See from plot: 
train %>% mutate(date = round_date(date, unit = "week")) %>%
  group_by(date) %>%
  summarize(rating = mean(rating)) %>%
  ggplot(aes(date, rating)) +
  geom_point() +
  geom_smooth()

# Date effect grouped per week
train <- mutate(train, date = round_date(date, unit = "week"))
str(train)
test <- mutate(test, date = round_date(date, unit = "week"))

# Calculate date effects
date_avgs <- train %>% 
  left_join(movie_avgs, by='movieId') %>%
  left_join(user_avgs, by='userId') %>%
  group_by(date) %>%
  summarize(b_d = mean(rating - mu - b_i - b_u))

date_avgs %>% qplot(b_d, geom ="histogram", bins = 10, data = ., color = I("black"))

# Predict the ratings
predicted_ratings <- test %>% 
  left_join(movie_avgs, by='movieId') %>%
  left_join(user_avgs, by='userId') %>%
  left_join(date_avgs, by='date') %>%
  mutate(pred = mu + b_i + b_u + b_d) %>%
  .$pred

# Calculate RMSE
rmse_m_u_d <- RMSE(predicted_ratings, test$rating)
rmse_m_u_d

# Table to compare RSME
rmse_results1 <- bind_rows(rmse_results1,
                           data_frame(method="Movie + User + Date Effects Model",  
                                      RMSE = rmse_m_u_d ))
rmse_results1 %>% knitr::kable()


##############################################################################################################
# Movie, User, Date and add Genre effect
# calculate genre effect
genre_avgs <- train %>% 
  left_join(movie_avgs, by='movieId') %>%
  left_join(user_avgs, by='userId') %>%
  left_join(date_avgs, by='date') %>%
  group_by(genres) %>%
  summarize(b_g = mean(rating - mu - b_i - b_u - b_d))

genre_avgs %>% qplot(b_g, geom ="histogram", bins = 10, data = ., color = I("black"))

# Predict the ratings
predicted_ratings <- test %>% 
  left_join(movie_avgs, by='movieId') %>%
  left_join(user_avgs, by='userId') %>%
  left_join(date_avgs, by='date') %>%
  left_join(genre_avgs, by='genres') %>%
  mutate(pred = mu + b_i + b_u + b_d + b_g) %>%
  .$pred

# Calculate RMSE
rmse_m_u_d_g <- RMSE(predicted_ratings, test$rating)
rmse_m_u_d_g

# Table to compare RSME
rmse_results1 <- bind_rows(rmse_results1,
                           data_frame(method="Movie + User + Date + Genres Effects Model",  
                                      RMSE = rmse_m_u_d_g ))
rmse_results1 %>% knitr::kable()



##############################################################################################################
# Movie, User and add Single_Genre effect
train2 <- train %>% mutate(Orig_genres=genres)  %>%
  separate(genres,c("single_genres"),sep = "\\|") 
str(train2)

test2 <- test %>% mutate(Orig_genres=genres)  %>%
  separate(genres,c("single_genres"),sep = "\\|")

# calculate genre effect
single_genre_avgs1 <- train2 %>% 
  left_join(movie_avgs, by='movieId') %>%
  left_join(user_avgs, by='userId') %>%
  group_by(single_genres) %>%
  summarize(b_g = mean(rating - mu - b_i - b_u))

single_genre_avgs1 %>% qplot(b_g, geom ="histogram", bins = 10, data = ., color = I("black"))

# Predict the ratings
predicted_ratings <- test2 %>% 
  left_join(movie_avgs, by='movieId') %>%
  left_join(user_avgs, by='userId') %>%
  left_join(single_genre_avgs1, by='single_genres') %>%
  mutate(pred = mu + b_i + b_u + b_g) %>%
  .$pred

# Calculate RMSE
rmse_m_u_sg <- RMSE(predicted_ratings, test2$rating)
rmse_m_u_sg

# Table to compare RSME
rmse_results1 <- bind_rows(rmse_results1,
                           data_frame(method="Movie + User + Single_Genres Effects Model",  
                                      RMSE = rmse_m_u_sg ))
rmse_results1 %>% knitr::kable()



##############################################################################################################
# Movie, User, Date and add Single_Genre effect
# calculate genre effect
single_genre_avgs2 <- train2 %>% 
  left_join(movie_avgs, by='movieId') %>%
  left_join(user_avgs, by='userId') %>%
  left_join(date_avgs, by='date') %>%
  group_by(single_genres) %>%
  summarize(b_g = mean(rating - mu - b_i - b_u - b_d))

single_genre_avgs2 %>% qplot(b_g, geom ="histogram", bins = 10, data = ., color = I("black"))

# Predict the ratings
predicted_ratings <- test2 %>% 
  left_join(movie_avgs, by='movieId') %>%
  left_join(user_avgs, by='userId') %>%
  left_join(date_avgs, by='date') %>%
  left_join(single_genre_avgs2, by='single_genres') %>%
  mutate(pred = mu + b_i + b_u + b_d + b_g) %>%
  .$pred

# Calculate RMSE
rmse_m_u_d_sg <- RMSE(predicted_ratings, test2$rating)
rmse_m_u_d_sg

# Table to compare RSME
rmse_results1 <- bind_rows(rmse_results1,
                           data_frame(method="Movie + User + Date + Single_Genres Effects Model",  
                                      RMSE = rmse_m_u_d_sg ))
rmse_results1 %>% knitr::kable()



##############################################################################################################
# Regularized movie and user effect method
# Create sequence of lambdas to test
lambdas <- seq(0, 10, 0.25)

# Build and test model for each lambda
rmses <- sapply(lambdas, function(l){
  mu <- mean(train$rating)
  b_i <- train %>%
    group_by(movieId) %>%
    summarize(b_i = sum(rating - mu)/(n()+l))
  b_u <- train %>% 
    left_join(b_i, by="movieId") %>%
    group_by(userId) %>%
    summarize(b_u = sum(rating - b_i - mu)/(n()+l))
  predicted_ratings <- 
    test %>% 
    left_join(b_i, by = "movieId") %>%
    left_join(b_u, by = "userId") %>%
    mutate(pred = mu + b_i + b_u) %>%
    .$pred
  return(RMSE(predicted_ratings, test$rating))
})

# Plot the RMSE for each lambda
qplot(lambdas, rmses)  

# Best lambda
lambda <- lambdas[which.min(rmses)]
lambda

# Best RMSE
min(rmses)

# table to compare RMSE
rmse_results1 <- bind_rows(rmse_results1,
                          data_frame(method="Regularized Movie + User Effect Model",  
                                     RMSE = min(rmses)))
rmse_results1 %>% knitr::kable()



##############################################################################################################
#Matrix factorization using recosystem

set.seed(1, sample.kind="Rounding") # if using R 3.6 or later

# Convert the data into the correct format for recosystem
train_matrix <- with(train, data_memory(user_index = userId,
                                        item_index = movieId,
                                        rating = rating))
test_matrix <- with(test, data_memory(user_index = userId,
                                      item_index = movieId,
                                      rating = rating))

#Create a model object (a Reference Class object in R) by calling Reco()
matrix <- Reco()

#(Optionally) call the $tune() method to select best tuning parameters along a set of candidate values
#################### WARNING!! This step takes about 15 - 20 minutes to complete###############################
opts_tune = matrix$tune(train_matrix,
                        opts = list(dim = c(10, 20, 30), 
                               costp_l2 = c(0.01, 0.1),
                               costq_l2 = c(0.01, 0.1),
                               costp_l1 = 0,
                               costq_l1 = 0,
                               lrate    = c(0.01, 0.1),
                               nthread  = 4,
                               niter    = 10,
                               verbose  = TRUE))

# Train the model by calling the $train() method. A number of parameters can be set inside the function, 
# coming from the result of $tune()
matrix$train(train_matrix, opts= c(opts_tune$min, nthread= 4, niter= 30))

# Use the $predict() method to compute predicted values
results_matrix <- matrix$predict(test_matrix, out_memory())

# Calculate RMSE
rmse_matrix <- RMSE(results_matrix, test$rating)
rmse_matrix

# Table to compare RMSE
rmse_results1 <- bind_rows(rmse_results1,
                           data_frame(method="Matrix factorisation Model",  
                                      RMSE = rmse_matrix))
rmse_results1 %>% knitr::kable()



##############################################################################################################
# Try the best 'simple model' (Movie + User + Date + Genre) on Final_holdout_test
# Mean train edx set
mu_edx <- mean(edx$rating)

# Movie effect edx train set
movie_avgs_edx <- edx %>% 
  group_by(movieId) %>% 
  summarize(b_i = mean(rating - mu))

# User effects edx train set
user_avgs_edx <- edx %>% 
  left_join(movie_avgs, by='movieId') %>%
  group_by(userId) %>%
  summarize(b_u = mean(rating - mu - b_i))

# Calculate date for edx and final_holdout_test sets
edx <- mutate(edx, date = as_datetime(timestamp))
final_holdout_test <- mutate(final_holdout_test, date = as_datetime(timestamp))

# Date effect grouped per week
edx <- mutate(edx, date = round_date(date, unit = "week"))
final_holdout_test <- mutate(final_holdout_test, date = round_date(date, unit = "week"))
str(edx)
str(final_holdout_test)

# Date effect edx train set
date_avgs_edx <- edx %>% 
  left_join(movie_avgs, by='movieId') %>%
  left_join(user_avgs, by='userId') %>%
  group_by(date) %>%
  summarize(b_d = mean(rating - mu - b_i - b_u))

# Genre effect edx train set
genre_avgs_edx <- edx %>% 
  left_join(movie_avgs, by='movieId') %>%
  left_join(user_avgs, by='userId') %>%
  left_join(date_avgs, by='date') %>%
  group_by(genres) %>%
  summarize(b_g = mean(rating - mu - b_i - b_u - b_d))

# Predict the ratings
predicted_ratings_edx <- final_holdout_test %>% 
  left_join(movie_avgs_edx, by='movieId') %>%
  left_join(user_avgs_edx, by='userId') %>%
  left_join(date_avgs_edx, by='date') %>%
  left_join(genre_avgs_edx, by='genres') %>%
  mutate(pred = mu + b_i + b_u + b_d + b_g) %>%
  .$pred

# Calculate RMSE
rmse_m_u_d_g_edx <- RMSE(predicted_ratings_edx, final_holdout_test$rating)
rmse_m_u_d_g_edx
# Lower than 0.86490, but we can probably do better with regularization and matrix factorization 
rmse_results_final <- data_frame(method = "Movie + User + Date + Genre", RMSE = rmse_m_u_d_g_edx)



##############################################################################################################
# Try the regularized movie and user effect method on Final_holdout_test
# Create sequence of lambdas to test
lambdas <- seq(0, 10, 0.25)

# Build and test model for each lambda
rmses_edx <- sapply(lambdas, function(l){
  mu <- mean(edx$rating)
  b_i <- edx %>%
    group_by(movieId) %>%
    summarize(b_i = sum(rating - mu)/(n()+l))
  b_u <- edx %>% 
    left_join(b_i, by="movieId") %>%
    group_by(userId) %>%
    summarize(b_u = sum(rating - b_i - mu)/(n()+l))
  predicted_ratings <- 
    final_holdout_test %>% 
    left_join(b_i, by = "movieId") %>%
    left_join(b_u, by = "userId") %>%
    mutate(pred = mu + b_i + b_u) %>%
    .$pred
  return(RMSE(predicted_ratings, final_holdout_test$rating))
})

# Plot the RMSE for each lambda
qplot(lambdas, rmses_edx)  

# Best lambda
lambda <- lambdas[which.min(rmses_edx)]
lambda

# Best RMSE
min(rmses_edx)

# table to compare RMSE
rmse_results_final <- bind_rows(rmse_results_final,
                           data_frame(method="Regularized Movie + User Effect Model",  
                                      RMSE = min(rmses_edx)))
rmse_results_final %>% knitr::kable()
# Also below the threshold and better than the not regularized model.



##############################################################################################################
# Try the matrix factorization method on Final_holdout_test
set.seed(1, sample.kind="Rounding") # if using R 3.6 or later

# Convert the data into the correct format for recosystem
edx_matrix <- with(edx, data_memory(user_index = userId,
                                        item_index = movieId,
                                        rating = rating))
final_holdout_test_matrix <- with(final_holdout_test, data_memory(user_index = userId,
                                      item_index = movieId,
                                      rating = rating))

#Create a model object (a Reference Class object in R) by calling Reco()
matrix_edx <- Reco()

#(Optionally) call the $tune() method to select best tuning parameters along a set of candidate values
#################### WARNING!! This step takes about 15 - 20 minutes to complete###############################
opts_tune = matrix_edx$tune(edx_matrix,
                        opts = list(dim = c(10, 20, 30), 
                                    costp_l2 = c(0.01, 0.1),
                                    costq_l2 = c(0.01, 0.1),
                                    costp_l1 = 0,
                                    costq_l1 = 0,
                                    lrate    = c(0.01, 0.1),
                                    nthread  = 4,
                                    niter    = 10,
                                    verbose  = TRUE))

# Train the model by calling the $train() method. A number of parameters can be set inside the function, 
# coming from the result of $tune()
matrix_edx$train(edx_matrix, opts= c(opts_tune$min, nthread= 4, niter= 30))

# Use the $predict() method to compute predicted values
results_matrix_edx <- matrix_edx$predict(final_holdout_test_matrix, out_memory())

# Calculate RMSE
rmse_matrix_edx <- RMSE(results_matrix_edx, final_holdout_test$rating)
rmse_matrix_edx

# Table to compare RMSE
rmse_results_final <- bind_rows(rmse_results_final,
                           data_frame(method="Matrix factorisation Model",  
                                      RMSE = rmse_matrix_edx))
rmse_results_final %>% knitr::kable()
# The matrix factorization method results in the lowest RMSE of ~0.7812

